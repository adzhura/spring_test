package com.spring;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;


@Component
@ConfigurationProperties
@Getter
@Setter
public class ApplicationConfig
{
    private String fileName;
    private String loginService;
    private String testProvider;
    private String testProcessor;
    private String testAnalizer;
}
