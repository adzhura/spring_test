package com.spring;


import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;


@Component
public class ApplicationMessages extends ReloadableResourceBundleMessageSource
{
    public ApplicationMessages()
    {
        super();
        
        setBasename("com/spring/i18n/messages");
        setDefaultEncoding("UTF-8");
    }
}
