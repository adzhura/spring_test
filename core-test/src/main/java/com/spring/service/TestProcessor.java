package com.spring.service;


import com.spring.domain.TestCase;
import com.spring.domain.User;


public interface TestProcessor
{
    TestCase process(User user, TestProvider testProvider);
}
