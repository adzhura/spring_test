package com.spring.service;


import com.spring.domain.User;


public interface LoginService
{
    User login() throws LoginException;
}
