package com.spring.service;


import com.spring.domain.TestCase;


public interface TestAnalizer
{
    void analize(TestCase testCase);
}
