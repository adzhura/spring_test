package com.spring.service;


import java.util.Collection;

import com.spring.domain.Test;


public interface TestProvider
{
    Collection<Test> getTests();
}
