package com.spring.service.impl;


import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencsv.CSVReader;
import com.spring.ApplicationConfig;
import com.spring.domain.Test;
import com.spring.service.TestProvider;


@Service
public class CSVTestProvider implements TestProvider
{
    private static final String CLASSPATH_PREFIX = "classpath:";
    private static final String FILE_PREFIX = "file:";

    private static Logger logger = LoggerFactory
        .getLogger(CSVTestProvider.class);

    private final String fileName;

    @Autowired
    public CSVTestProvider(ApplicationConfig cfg)
    {
        this.fileName = cfg.getFileName();
    }

    @Override
    public Collection<Test> getTests()
    {
        ArrayList<Test> result = new ArrayList<>();

        try (CSVReader reader = new CSVReader(getReader()))
        {
            String[] line;
            int lineNumber = 0;

            while ((line = reader.readNext()) != null)
            {
                lineNumber++;
                if (line.length == 2)
                {
                    Test test = createTest(line[0].trim(), line[1].trim());
                    result.add(test);

                    logger.debug("Parsed line: {}", test);
                }
                else
                {
                    logger.warn("Incorrect format in line: {}", lineNumber);
                }
            }
        }
        catch (Exception e)
        {
            logger.error("File read error: {}", fileName, e);
        }

        return result;
    }

    private Reader getReader() throws Exception
    {
        if (fileName.startsWith(CLASSPATH_PREFIX))
        {
            logger.debug("Read resource: {}", fileName);
            return new InputStreamReader(
                getClass().getClassLoader().getResourceAsStream(
                    fileName.substring(CLASSPATH_PREFIX.length())));
        }
        else
        {
            logger.debug("Read file: {}", fileName);
            return new FileReader(fileName.startsWith(FILE_PREFIX)
                ? fileName.substring(FILE_PREFIX.length())
                : fileName);
        }
    }

    protected Test createTest(String question, String answer)
    {
        return new Test(question, answer);
    }
}
