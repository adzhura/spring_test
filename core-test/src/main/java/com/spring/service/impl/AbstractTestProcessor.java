package com.spring.service.impl;


import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Function;

import com.spring.domain.Test;
import com.spring.domain.TestAnswer;
import com.spring.domain.TestCase;
import com.spring.domain.User;
import com.spring.service.TestProcessor;
import com.spring.service.TestProvider;


public abstract class AbstractTestProcessor
        implements TestProcessor, Function<Test, TestAnswer>
{
    @Override
    public TestCase process(User user, TestProvider testProvider)
    {
        Collection<Test> tests = testProvider.getTests();

        if (tests != null && tests.size() > 0)
        {
            ArrayList<TestAnswer> result = new ArrayList<>();

            for (Test test : tests)
            {
                result.add(apply(test));
            }

            return new TestCase(user, result);
        }

        return null;
    }
}
