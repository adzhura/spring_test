package com.spring.service.impl;


import java.util.Locale;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.spring.ApplicationMessages;
import com.spring.domain.User;
import com.spring.service.LoginException;
import com.spring.service.LoginService;


@Service
public class ConsoleLoginService implements LoginService
{
    private final MessageSource messages;

    @Autowired
    public ConsoleLoginService(ApplicationMessages messages)
    {
        this.messages = messages;
    }

    @Override
    public User login() throws LoginException
    {
        try
        {
            User user = new User();

            System.out.println(
                messages.getMessage("login.prompt", null, Locale.getDefault()));
            prompt(
                messages.getMessage("login.first.name", null, Locale.getDefault()),
                s -> user.setFirstName(s));
            prompt(
                messages.getMessage("login.last.name", null, Locale.getDefault()),
                s -> user.setLastName(s));

            return user;
        }
        catch (Throwable t)
        {
            throw new LoginException(t);
        }
    }

    private void prompt(String prompt, Consumer<String> consumer)
    {
        String s = null;

        while (s == null || s.trim().length() == 0)
        {
            System.out.print(prompt + ": ");
            s = System.console().readLine();
        }

        consumer.accept(s.trim());
    }
}
