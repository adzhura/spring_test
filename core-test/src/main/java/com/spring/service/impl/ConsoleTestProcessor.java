package com.spring.service.impl;


import org.springframework.stereotype.Service;

import com.spring.domain.Test;
import com.spring.domain.TestAnswer;


@Service
public class ConsoleTestProcessor extends AbstractTestProcessor
{
    @Override
    public TestAnswer apply(Test t)
    {
        System.out.println(t.getQuestion());

        return new TestAnswer(t, System.console().readLine(">").trim());
    }
}
