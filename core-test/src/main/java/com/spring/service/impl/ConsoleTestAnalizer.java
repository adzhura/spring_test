package com.spring.service.impl;


import java.util.Collection;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.spring.ApplicationMessages;
import com.spring.domain.TestAnswer;
import com.spring.domain.TestCase;
import com.spring.domain.User;
import com.spring.service.TestAnalizer;


@Service
public class ConsoleTestAnalizer implements TestAnalizer
{
    private static Logger logger = LoggerFactory
        .getLogger(ConsoleTestAnalizer.class);

    private final MessageSource messages;

    @Autowired
    public ConsoleTestAnalizer(ApplicationMessages messages)
    {
        this.messages = messages;
    }

    @Override
    public void analize(TestCase testCase)
    {
        if (testCase != null)
        {
            int passed = 0;

            User user = testCase.getUser();
            Collection<TestAnswer> testResults = testCase.getUserAnswers();

            for (TestAnswer testAnswer : testResults)
            {
                logger.info(
                    "test\nQuestion: {}\nAnswer: {}\nCorrect answer: {}",
                    new Object[] {testAnswer.getTest().getQuestion(),
                        testAnswer.getAnswer(),
                        testAnswer.getTest().getAnswer()});

                if (testAnswer.getTest().getAnswer()
                    .equalsIgnoreCase(testAnswer.getAnswer()))
                {
                    passed++;
                }
            }

            logger.info("{} {} successfully answered on {} of {} questions",
                new Object[] {user.getFirstName(), user.getLastName(), passed,
                    testResults.size()});
            System.out.println(String.format(
                "%1$s %2$s, " + messages.getMessage("result.prompt",
                    new Object[] {passed, testResults.size()},
                    Locale.getDefault()),
                user.getFirstName(), user.getLastName()));
        }

        System.console()
            .readLine(messages.getMessage("buy", null, Locale.getDefault()));
    }
}
