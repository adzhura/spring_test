package com.spring.service.impl;


import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.spring.domain.User;
import com.spring.service.LoginException;
import com.spring.service.LoginService;
import com.spring.service.TestAnalizer;
import com.spring.service.TestExecutor;
import com.spring.service.TestProcessor;
import com.spring.service.TestProvider;


@Service
public class DefaultTestExecutor implements TestExecutor
{
    private static Logger logger = LoggerFactory
        .getLogger(DefaultTestExecutor.class);

    @Resource(name = "${loginService}")
    private LoginService loginService;

    @Resource(name = "${testProvider}")
    private TestProvider testProvider;

    @Resource(name = "${testProcessor}")
    private TestProcessor testProcessor;

    @Resource(name = "${testAnalizer}")
    private TestAnalizer testAnalizer;

    @Override
    public void run()
    {
        try
        {
            User user = loginService.login();
            testAnalizer.analize(testProcessor.process(user, testProvider));
        }
        catch (LoginException e)
        {
            logger.error("Error login", e);
        }
    }
}
