package com.spring.service.impl;


import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.spring.ApplicationConfig;
import com.spring.ApplicationMessages;
import com.spring.domain.Test;


@Service
public class LocalizedCSVTestProvider extends CSVTestProvider
{
    private final MessageSource messages;

    @Autowired
    public LocalizedCSVTestProvider(ApplicationConfig cfg,
        ApplicationMessages messages)
    {
        super(cfg);
        this.messages = messages;
    }

    @Override
    protected Test createTest(String question, String answer)
    {
        return super.createTest(
            messages.getMessage(question, null, Locale.getDefault()),
            messages.getMessage(answer, null, Locale.getDefault()));
    }

}
