package com.spring.domain;


import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class Test
{
    private String question;

    private String answer;

    @Override
    public String toString()
    {
        return "Question: " + question + "; Answer: " + answer;
    }
}
