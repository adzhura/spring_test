package com.spring.domain;


import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class TestAnswer
{
    private Test test;

    private String answer;
}
