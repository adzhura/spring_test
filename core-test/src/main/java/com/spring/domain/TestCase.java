package com.spring.domain;


import java.util.Collection;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class TestCase
{
    private User user;

    private Collection<TestAnswer> userAnswers;
}
