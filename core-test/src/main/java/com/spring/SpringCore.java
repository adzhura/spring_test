package com.spring;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.spring.service.TestExecutor;


@SpringBootApplication
public class SpringCore
{
    public static void main(String[] args)
    {
        try (ConfigurableApplicationContext ctx = SpringApplication.run(SpringCore.class, args))
        {
            ctx.getBean(TestExecutor.class).run();
        }
    }
}
