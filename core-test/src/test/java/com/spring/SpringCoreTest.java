package com.spring;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.spring.service.TestProvider;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringCoreTest
{
    @Autowired
    @Qualifier("CSVTestProvider")
    TestProvider testProvider;

    @Autowired
    @Qualifier("localizedCSVTestProvider")
    TestProvider localizedTestProvider;

    @Test
    public void testProvider()
    {
        testProvider(testProvider, "q1", "a1");
    }

    @Test
    public void localizedTestProvider()
    {
        testProvider(localizedTestProvider, "q1_loc", "a1_loc");
    }

    private void testProvider(TestProvider testProvider,
        String expectedQuestion, String expectedAnswer)
    {
        assertNotNull(testProvider.getTests());
        assertEquals(testProvider.getTests().size(), 1);

        Iterator<com.spring.domain.Test> iterator = testProvider.getTests()
            .iterator();

        assertTrue(iterator.hasNext());
        com.spring.domain.Test next = iterator.next();
        assertEquals(next.getQuestion(), expectedQuestion);
        assertEquals(next.getAnswer(), expectedAnswer);
    }
}
